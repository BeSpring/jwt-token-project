package com.techprimers.security.jwtsecurity.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.techprimers.security.jwtsecurity.entities.User;

public interface UserService extends CrudRepository<User, Long> {
	@Query("select u from User u where u.email=:email and u.password=:password and u.role=:role")
	public User userExist(@Param("email") String email,@Param("password") String password,@Param(value = "role") String role);

}
