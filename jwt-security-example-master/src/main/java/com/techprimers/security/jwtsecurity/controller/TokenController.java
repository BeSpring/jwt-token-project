package com.techprimers.security.jwtsecurity.controller;

import com.techprimers.security.jwtsecurity.entities.User;
import com.techprimers.security.jwtsecurity.repositories.UserService;
import com.techprimers.security.jwtsecurity.security.JwtGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/token")
public class TokenController {
	@Autowired
	UserService userService;

	private JwtGenerator jwtGenerator;

	public TokenController(JwtGenerator jwtGenerator) {
		this.jwtGenerator = jwtGenerator;
	}

	@PostMapping
	public String generate(@RequestBody final User jwtUser) {
		User user=this.userService.userExist(jwtUser.getEmail(),jwtUser.getPassword(),jwtUser.getRole());
		if (user!=null)
			return jwtGenerator.generate(user);
		else
			return "User doesn't exist";

	}
}
