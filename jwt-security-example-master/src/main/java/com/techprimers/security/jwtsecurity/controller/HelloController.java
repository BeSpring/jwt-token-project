package com.techprimers.security.jwtsecurity.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techprimers.security.jwtsecurity.entities.User;
import com.techprimers.security.jwtsecurity.repositories.UserService;

@RestController
@RequestMapping("/rest/hello")
@PreAuthorize("hasRole('ADMIN')")
public class HelloController {
@Autowired UserService userService;
    @GetMapping("/pp")
    public String hello() {
        return "Hello World";
    }
    @PostMapping("/create")
    public User create(@RequestBody User user) {
        return this.userService.save(user);
    }
}
