package com.techprimers.security.jwtsecurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cetto")
public class CettoController {
	@GetMapping("/hi")
	public String hello() {
		return "Hi!";
	}
}
